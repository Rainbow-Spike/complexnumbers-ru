---
title: 'Complex Numbers: электронная поп-музыка для интеллектуалов'
description: 'Поём про восприятие реальности, крионику, трансгуманизм, метаэтику, и советских школьниц из будущего.'

image: 'https://complexnumbers.ru/images/logo.png'
twitterTitle: 'Complex Numbers: научная фантастика'
twitterDescription: 'Музыка и видео, оперы на русском языке. Философия, будущее, сознание, работа мозга, восприятие реальности.'

schema:
  '@context': 'http://schema.org'
  '@type': MusicGroup
  name: Complex Numbers
  alternateName: Комплексные Числа
  url: 'https://complexnumbers.ru/'
  logo: 'https://complexnumbers.ru/images/logo.png'
  sameAs:
    - 'https://www.youtube.com/channel/UCAYH-z9hCpzMZAEAHV6WpXA'
    - 'https://open.spotify.com/artist/3PBUz26WYPxZo1uFbS2RbV'
    - 'https://complexnumbers.bandcamp.com/'
    - 'https://music.apple.com/artist/complex-numbers/1140302618'
    - 'https://music.amazon.com/artists/B01JKKORCM'
    - 'https://www.last.fm/music/Complex+Numbers'
    - 'https://musicbrainz.org/artist/d2062464-9c2e-4d1f-8005-dac138da03e6'
    - 'https://www.discogs.com/artist/1439159'
    - 'https://vk.com/viktorargonovproject'
    - 'https://www.wikidata.org/wiki/Q100269171'
---

Complex Numbers (включая Виктор Аргонов Project) — российская музыкальная группа, работающая в разных форматах от отдельных треков до симфоний и опер. Основное наше кредо — сочетание доступной мелодичной электронной музыки (вплоть до намеренной эксплуатации поп-штампов) и сложных интеллектуальных смыслов научно-технического и философско-футурологического плана. В силу этого сочетания, на современной сцене проект занимает почти пустую нишу. Вся музыка группы и материалы сайта распространяются под лицензией [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode.ru). Смотрите также [Черновик статьи на Википедии](https://ru.wikipedia.org/wiki/Участник:Vitaly_Zdanevich/Complex_Numbers_(музыкальная_группа)), [Commons](https://commons.wikimedia.org/wiki/Category:Complex_Numbers_(musical_group)), [Wikidata](https://www.wikidata.org/wiki/Q100269171), [Telegram group](https://t.me/complexnumbersgroup)

## Наградить группу

<div style="text-align: left;">

<nobr>Тинькофф 5536 9140 2294 7703 VIKTOR ARGONOV</nobr>, 
<nobr>Сбер 4276 1609 6787 1493 Виктор Юрьевич А</nobr>, 
<nobr>YooMoney 41001159842314</nobr>, 
<nobr>Qiwi 79024826592</nobr>, 
<nobr>PayPal wadimkem<span>@</span>gmail.com (comment: CN)</nobr>, 
<nobr>Bitcoin 1QEGaVdZ7wAhe6uAd4ARVhbi81tK9XXgjT</nobr>, 
<nobr>Etherium 0xFb2Bf4391E9835F408c64a9dDF39AA5AE22cd76D</nobr>

</div>

## Дискография и видеография

<section class="albumblock">
<a href="/2084/#content"><img src="/images/covers/2084-wip.png" class="albumcover-small" width="256px" height="256px" /></a>
<div class="albumdescription">

### [2084. Техно-опера (2021–20XX, в работе)](/2084/#content)

Третья электронная опера Виктора Аргонова. Мир, где люди смогли уничтожить в себе конфликт приятного и полезного, и почти постоянно счастливы, как живые персонажи "Мы, 22". Но общая жизнь больше напоминает фантазии "отличников" позднего СССР: мир, где войн нет, где труд приносит радость, где значительная часть населения трудится в НИИ, хотя и за некоторых просто "вкалывают роботы". У такой жизни есть обратная сторона: общество помешано на безопасности, в нём введена слежка всех за всеми, руководящие органы в значительной степени децентрализованы. Оправданы ли эти запреты, и как общество отреагирует, если кто-то их нарушит?

</div></section>

<section class="albumblock">
<div>
<img src="/images/covers/btta-blurred.jpg" class="albumcover-small" width="256px" />
</div>
<div class="albumdescription">

### Песни для нового альбома (2016–2022, в работе)&emsp;<span class="normal">[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378456/packages/generic/release/1.0.0/2022-between-the-two-ages-mp3.zip)&emsp;[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72220826)</span>

[Межкарантинное лето](http://commons.wikimedia.org/wiki/File:Summer-between-quarantines.flac)
&emsp;[Разбор смысла](https://vk.com/@viktorargonovproject-2011-11-18)\
[В дни чудес <small>(Три бозона)</small>](http://commons.wikimedia.org/wiki/File:In-the-days-of-wonder.flac) <sup>[1]</sup>
&emsp;[Разбор смысла](https://vk.com/@viktorargonovproject-complex-numbers-v-dni-chudes-tri-bozona-smysl-pesni)\
[Пролегла черта <small>(Меж двух эпох)</small>](https://commons.wikimedia.org/wiki/File:The-line-has-been-drawn.flac) <sup>[1]</sup>\
[Пробуждение <small>(Никто не решал, кем родиться) [Версия 2020]</small>](https://commons.wikimedia.org/wiki/File:Awakening-2020-version.flac)\
[200 минут <small>(Плен инстинктов жесток) [Версия 2020]</small>](http://commons.wikimedia.org/wiki/File:200-minutes-2020-version.flac) <sup>[3]</sup>\
[1000 лет назад <small>(Мне звёзды казались так близко)</small>](http://commons.wikimedia.org/wiki/File:1000-years-ago.flac)
&emsp;[История и обсуждение](https://argonov.livejournal.com/227741.html)\
[Сердцу прикажешь <small>(К чувствам ключи)</small>](https://commons.wikimedia.org/wiki/File:The-heart-will-obey-you.flac)
&emsp;[История и обсуждение](https://argonov.livejournal.com/227383.html)\
[Время придёт <small>(Изучает прогресс поведение людей)</small>](https://commons.wikimedia.org/wiki/File:The-time-will-come.flac) <sup>[2]</sup>
 [История и обсуждение](https://argonov.livejournal.com/204039.html)\
Музыка и текст: [Виктор Аргонов](https://www.wikidata.org/wiki/Q105755025).\
Вокал: [Len](https://www.wikidata.org/wiki/Q105906250), кроме: [1] [Oleviia](https://www.wikidata.org/wiki/Q106182851), [2] Len, [Ariel](https://www.wikidata.org/wiki/Q105905487), [3] [Елена Яковец](https://www.wikidata.org/wiki/Q105962641), Len

</div></section>

<section class="albumblock">
<a href="/good-fairy-tales/#content"><img src="/images/covers/good-tales.jpg" class="albumcover-small" width="256px" height="256px" /></a>
<div class="albumdescription">

### [Сказки добрые. Каверы-переосмысления (2003–2021)](/good-fairy-tales/#content)&emsp;<span class="normal">[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378455/packages/generic/release/1.0.0/2021-good-fairy-tales-mp3.zip)&emsp;[VK](https://vk.com/wall-23865151_34082)</span>

11 треков разных лет, в том числе со страницами обсуждений:  
Сказки добрые <small>(Каролина Trance Cover)</small>  [История и обсуждение](https://argonov.livejournal.com/202947.html)  
8 минут <small>(Игорь Николаев Trance Cover)</small>  [История и обсуждение](https://argonov.livejournal.com/228740.html)

Переработка музыки и текстов: Виктор Аргонов. Вокал: Ariel, Len, Oleviia

</div></section>


<section class="albumblock">  
<a href="/we-22nd-century/#content"><img src="/images/covers/we256-2.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Мы, XXII век. Техно-опера (видео, 2017–2018)](/we-22nd-century/#content)

Анимационная экранизация одноимённой оперы. Серьёзно раскрывает смысл.

Видео, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://www.youtube.com/watch?v=YrXk2buqsgg)
&emsp;&emsp;
[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378452/packages/generic/release/1.0.0/2018-we-22nd-century-mp3.zip) и [видео](https://drive.google.com/drive/folders/14NGOahPtzJhCosv271GmO1Kexfx4DNUe)
&emsp;&emsp;
[Обсуждение](https://argonov.livejournal.com/223011.html)

</div></section>


<section class="albumblock">  
<a href="/we-22nd-century/#content"><img src="/images/covers/we402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Мы, XXII век. Техно-опера (аудио, 2016–2018)](/we-22nd-century/#content)

Рассказ о человеке XX века, попавшем в XXII век через криозаморозку. Новое общество считает себя раем. Но с позиции идеалов XX века в нём есть антиутопические черты, включая размытие понятия личности. Причём столь глубокое и хитро обоснованное, что Замятин, Хаксли и Оруэлл курят в сторонке.

Музыка: Андрей Климковский, Виктор Аргонов. Текст, аранжировка: Виктор Аргонов. Вокал: Len, Ariel.

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378452/packages/generic/release/1.0.0/2018-we-22nd-century-mp3.zip)
&emsp;&emsp;
[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_83082486)
&emsp;&emsp;
[Обсуждение](https://argonov.livejournal.com/214695.html)
&emsp;&emsp;
[Вокал и фонограммы](https://gitlab.com/complexnumbers/we22-src/-/archive/master/we22-src-master.zip)

</div></section>


<section class="albumblock">
<div class="albumdescription">

### Каверы и ремиксы других музыкантов на нас &nbsp;&nbsp;&nbsp;&nbsp;<span class="normal">[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_64748401)&nbsp;&nbsp;&nbsp;&nbsp;[zip](https://gitlab.com/complexnumbers/covers/-/archive/main/covers-main.zip?path=files)&nbsp;&nbsp;&nbsp;&nbsp;[Commons](https://commons.wikimedia.org/wiki/Category:Complex_Numbers_(musical_group):_remixes_and_covers)</span>

Время придёт (Tycoos remix), Там, за чертой (DIP Project remix), Там, за чертой (Bananafox remix), Свой путь (Tycoos remix),  
Сделать шаг (Reaktivz remix), Там, за чертой (Sonar Spray cover), Предания неведомых миров (Isdays cover).

</div></section>


<section class="albumblock">  
<a href="/reth/#content"><img src="/images/covers/re402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Переосмысляя прогресс. Техно-симфония (видео, 2013–2015)](/reth/#content)

Видеосинопсис одноимённой симфонии. Необходим для понимания смысла.

Видео, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://youtu.be/EpVd-TXDUk4)  [Скачать](https://drive.google.com/file/d/12HBnrzhK5pA6Af_G5is8Imld2V3pLlkF/view?usp=sharing)

</div></section>


<section class="albumblock">  
<a href="/reth/#content"><img src="/images/covers/prog-402.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">

### [Переосмысляя прогресс. Техно-симфония (аудио, 2013–2015)](/reth/#content)

Философский рассказ о прошлой и будущей истории отношений человека и технологий: восхищение "прогрессом ради прогресса", переосмысление идеалов, попытки бегства, новые открытия и реальное духовное преображение.

Музыка, текст, аранжировка: Виктор Аргонов. Звук: [Саян Андриянов](https://www.wikidata.org/wiki/Q108058755). Вокал: Елена Яковец, Len, Ariel.

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378450/packages/generic/release/1.0.0/2015-rethinking-progress-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/reth-src/-/archive/master/reth-src-master.zip)

</div></section>

<section class="albumblock">  
<a href="/merm/#content"><img src="/images/covers/merm-402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Русалочка. Техно-опера (2005–2013)](/merm/#content)

Фантастическое переосмысление андерсеновского сюжета в условиях современной России.

Музыка, текст, аранжировка: Виктор Аргонов. Вокальные роли: Ariel, Александра Пряха, [Николай Перминов](https://www.wikidata.org/wiki/Q105943304), [Виталий Трофименко](https://www.wikidata.org/wiki/Q105943505). Речевые роли: [Александр Солнышкин](https://www.wikidata.org/wiki/Q105943935), [Виталий Зданевич](https://www.wikidata.org/wiki/Q105704549), Виктор Аргонов.

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378447/packages/generic/release/1.0.0/2013-the-little-mermaid-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/mermaid-src/-/archive/master/mermaid-src-master.zip?path=src-basic)

</div></section>


<section class="albumblock">  
<a href="/2032/#content"><img src="/images/covers/2032-402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [2032. Легенда о несбывшемся грядущем. Техно-опера (1999–2007)](/2032/#content)

Альтернативная история СССР 2032 года.

Музыка, текст, аранжировка: Виктор Аргонов. Гитарные партии: [Александр Асинский](https://www.wikidata.org/wiki/Q106489417). Вокальные роли: Виталий Трофименко, Ariel, Елена Яковец. Речевые роли: [Денис Шамрин](https://www.wikidata.org/wiki/Q106094199), [Евгений Гриванов](https://www.wikidata.org/wiki/Q109803279), [Андрей Иванов](https://www.wikidata.org/wiki/Q109803629), [Дмитрий Гончар](https://www.wikidata.org/wiki/Q106093180), [Сергей Гончаров](https://www.wikidata.org/wiki/Q109803729).

**[Слушать на сайте](/listen/2032/ru/)**  
[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378442/packages/generic/release/1.0.0/2007-2032-legend-of-a-lost-future-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/2032-src/-/archive/master/2032-src-master.zip?path=src-basic)

</div></section>

<section class="albumblock">  
<div class="albumdescription">

### Каверы на музыку игр nintendo, не вошедшие в альбомы (1996–2008)  <span class="normal">[VK](https://vk.com/audio?z=audio_playlist-23865151_76950283/8b72fafc583853b652) [zip](https://gitlab.com/api/v4/projects/32378457/packages/generic/release/1.0.0/2023-nintendo-covers-mp3.zip)</span>

Salvage Chute (feat Konami — Bucky O'Hare),\
Parting (feat Tecmo — Ninja Gaiden 3),\
Escape (feat. Konami — Bucky O'Hare),\
Again with You (feat Tecmo — Ninja Gaiden 3),\
Last Step to Victory (feat Technos — Double Dragon 2),\
2029 A. D. (feat Natsume — Shadow of the Ninja),\
Desert Forsage (feat Tecmo — Ninja Gaiden 3),\
Peaceful Sky (feat Konami — Super Contra),\
Ninja Tsoy (feat Tecmo — Ninja Gaiden 3; Кино; Сектор Газа).

</div></section>

<section class="albumblock">  
<img src="/images/covers/in402.jpg" class="albumcover-small" width="256px" />
<div class="albumdescription">

### Неизбежность (видео, 2008–2009)

Музыкальное видео на одноимённую песню из альбома "Последнее кольцо". Первое видео группы. Существенно раскрывает смысл песни.

Музыка, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://www.youtube.com/watch?v=FMJNta-okRw)   [Скачать видео](https://drive.google.com/drive/folders/1l1Jt0EWPuOAe35JO6WlOONzUEiqhOk-P)

</div></section>


<section class="albumblock">  
<a href="/last-ring/#content"><img src="/images/covers/la402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">

### [Последнее кольцо (2007–2009)](/last-ring/)

Третий альбом Complex Numbers.

Музыка: Виктор Аргонов, Андрей Климковский, Александр Асинский, Hydrogen. Текст: Виктор Аргонов, Ariel. Гитарные партии: Александр Асинский. Вокал: Ariel, [Наталья Митрофанова](https://www.wikidata.org/wiki/Q105942454).

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378444/packages/generic/release/1.0.0/2009-last-ring-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/last-src/-/archive/master/last-src-master.zip?path=src-basic)

[Слушать главную песню альбома на Commons](https://commons.wikimedia.org/wiki/File:Complex-numbers--last-ring.opus)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225350)

</div></section>


<section class="albumblock">  
<a href="/the-morning-of-the-new-millennium/#content"><img src="/images/covers/mo402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">  

### [Утро нового тысячелетия (1998–2003)](/the-morning-of-the-new-millennium/#content)

Второй альбом Complex Numbers.

Музыка: Виктор Аргонов, Александр Асинский, [Илья Пятов](https://www.wikidata.org/wiki/Q106314773), [Фрол Запольский](https://www.wikidata.org/wiki/Q102362017). Текст: Виктор Аргонов. Вокал: Наталья Митрофанова.

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378434/packages/generic/release/1.0.0/2003-the-morning-of-the-new-millennium-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/morn-src/-/archive/master/morn-src-master.zip?path=src-basic)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225328)

</div></section>


<section class="albumblock">  
<a href="/earths-attraction/#content"><img src="/images/covers/ea402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">  

### [Земное притяжение (1996–2002)](/earths-attraction/#content)

Первый альбом Complex Numbers.

Музыка: Фрол Запольский, Виктор Аргонов, Илья Пятов, Александр Асинский. Текст: Виктор Аргонов, Илья Пятов. Вокал: Наталья Митрофанова.

[Скачать mp3 в zip](https://gitlab.com/api/v4/projects/32378433/packages/generic/release/1.0.0/2002-earths-attraction-mp3.zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/eart-src/-/archive/master/eart-src-master.zip?path=src-basic)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225293)

</div></section>


<div style="text-align: right">

[Git репозиторий этого сайта](https://gitlab.com/complexnumbers/complexnumbers-ru)

</div>
