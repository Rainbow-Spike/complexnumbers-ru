---
title: 'Земное притяжение — Complex Numbers'
description: Первый альбом Complex Numbers.
image: '/images/covers/ea402r.jpg'

album:
  title: Земное притяжение (1996–2002)
  description: Первый альбом Complex Numbers.
  artist: Complex Numbers
  cover: /images/covers/ea402r.jpg
  credits: |
    Музыка: Фрол Запольский, Виктор Аргонов, Илья Пятов, Александр Асинский. Текст: Виктор Аргонов, Илья Пятов. Вокал: Наталья Митрофанова.

  mp3: https://gitlab.com/api/v4/projects/32378433/packages/generic/release/1.0.0/2002-earths-attraction-mp3.zip
  srcBasic: https://gitlab.com/complexnumbers/eart-src/-/archive/master/eart-src-master.zip?path=src-basic
  vk: https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225293
  bandcamp: https://complexnumbers.bandcamp.com/album/earths-attraction
  gdrive: https://drive.google.com/drive/folders/1f_maXEyiWWFbkaydv6dDslPam0qrOp3y
  youtube: https://www.youtube.com/playlist?list=OLAK5uy_llMgov6S43qPjUUpH0qfuz5U15B-BYNG8
  apple: https://music.apple.com/album/1140635186
  amazon: https://www.amazon.com/dp/B01JKKMYRW
  spotify: https://open.spotify.com/album/6hIDxFDDMZVDRtrFPAwWnp
  yandex: https://music.yandex.ru/album/3656694

  tracks:
  - title: Вступление
    description: Музыка - Евгений Строкин, Виктор Аргонов. 1997, полутораминутное инструментальное intro в стиле new age
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/01-introduction.mp3
  - title: Пути цивилизаций [Версия 2002]
    description: Музыка - Фрол Запольский. 1997, инструментальная композиция, органично соединившая в себе элементы techno начала 90-х и более позднего dream-house
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/02-ways-of-civilizations-2002-version.mp3
  - title: Над землёю алеет закат
    description: Музыка, текст - Виктор Аргонов, вокал - Наталья Митрофанова. 1996 - 1997, песня о любви, разлуке, далёком космосе и лоренцевских сокращениях
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/03-sunset-over-earth.mp3
  - title: Солнечный дождь
    description: Музыка - Виктор Аргонов. 1996 - 1997, первый инструментальный эксперимент в стиле new age. Достаточно мелодичный, но местами ещё пока грубовато звучащий
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/04-sunny-rain.mp3
  - title: Подводный туннель
    description: Музыка - Aster. 1997, Инструментальная композиция в стиле trance
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/05-underwater-tunnel.mp3
  - title: Разбег
    description: Музыка - Фрол Запольский
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/06-running-start.mp3
  - title: Тайфун
    description: Музыка - Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/07-typhoon.mp3
  - title: Минбарский снег
    description: Музыка, текст - Виктор Аргонов, вокал - Наталья Митрофанова. Песня, написанная под впечатлением сериала Вавилон-5, о беженцах на Минбар времён гражданской войны и диктатуры Кларка на Земле
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/08-minbari-snow.mp3
  - title: Ускорение [Версия 1998]
    description: Музыка - Aster
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/09-acceleration-1998-version.mp3
  - title: Конвеер
    description: Музыка - Фрол Запольский
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/10-conveyor.mp3
  - title: В полёте
    description: Музыка - Илья Пятов
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/11-in-the-flight.mp3
  - title: Две системы
    description: Музыка - Aster
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/12-two-systems.mp3
  - title: Земное притяжение
    description: Музыка, текст - Илья Пятов, вокал - Наталья Митрофанова
    url: https://complexnumbers.gitlab.io/releases/earths-attraction/mp3/13-earths-attraction.mp3
---
