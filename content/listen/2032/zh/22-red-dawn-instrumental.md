---
title: 红色黎明
subtitle: 纯音乐
image: /listen/2032/cover-clean.jpg
translation: https://www.bilibili.com/audio/au2036127
translator: Elizabeth
byArtist:
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Aleksandr Asinksy
  role: 吉他伴奏
- name: Firey-Flamy
  role: 俄译英
---
