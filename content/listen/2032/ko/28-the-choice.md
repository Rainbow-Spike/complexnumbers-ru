---
title: 선택
translation: http://transural.egloos.com/314852
translator: GRU Fields
byArtist:
- name: 아리엘
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: 빅토르 트로피멘코
  role: 아나톨리 밀리넵스키
  image: /images/photos/vit_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---

**ASGU:** 그래서, 넌 내 말에 계속 동의 한다는 것이지 - 삶의 가치라는 것은 결국 상대적라는걸... 뭐 유감이야. 만약에 내가 옳았 던 것이라면.. 그래... 인간이라는 것들.. 지구의 어느 면에서든.. "역사적 유물론"이라는게 대체 뭐야..? 결국엔 너마저도 극단적 선택이라는 요술지팡이으로 유물-기술적 개념을 대체할수 있다고 믿고 있어.. 기독교적 낙원을 지상에 형성할것이라고 말이지..

**밀리넵스키:** ... 나는 이미 굳게 결정했어..

**ASGU:** 뭐... 그럼.. 코드를 넣어줘, 절차시행을 위해.

나도 어짜피 이미 내가 있어야 할 곳이 어디인지 결정했어..
