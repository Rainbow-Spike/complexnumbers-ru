---
title: Ангел жизни новой
subtitle: ария Милиневского
byArtist:
- name: Виталий Трофименко
  role: Анатолий Милиневский
  image: /images/photos/vit_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Милиневский**

&emsp;&emsp;&emsp;Казалось мне всегда,<br>
&emsp;&emsp;&emsp;Что наша цель ясна,<br>
Что знаем мы, что ищем за порогом.<br>
&emsp;&emsp;&emsp;Оправдана и ложь.<br>
&emsp;&emsp;&emsp;Когда она нужна<br>
Для блага истины искомого итога.<br>
&emsp;&emsp;&emsp;И жизнями людей,<br>
&emsp;&emsp;&emsp;Мотивами их дел<br>
Сама судьба нас к цели направляет,<br>
&emsp;&emsp;&emsp;Так как же может быть,<br>
&emsp;&emsp;&emsp;Что эту цель теперь<br>
Наш рукотворный бог, по сути, отвергает?

Ведь ты была<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Сотворена<br>
&emsp;&emsp;&emsp;Как ангел жизни новой!<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Твоя весна<br>
За собой позвала<br>
&emsp;&emsp;&emsp;К рассвету дня иного!..<br>

Винить тебя<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Было б странно теперь:<br>
&emsp;&emsp;&emsp;Ты слишком много знаешь:<br>
Но как пройти<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Этот путь без потерь.<br>
&emsp;&emsp;&emsp;Что нам ты возвещаешь?

&emsp;&emsp;&emsp;Наш век ещё не прост,<br>
&emsp;&emsp;&emsp;Он противоречив:<br>
В нём счастье - лишь иллюзия порою:<br>
&emsp;&emsp;&emsp;Возможно, что и ты<br>
&emsp;&emsp;&emsp;Иллюзии нам дашь,<br>
Но что-то важное оставишь за спиною.<br>
&emsp;&emsp;&emsp;Ведь если ты права,<br>
&emsp;&emsp;&emsp;Технический прогресс<br>
Давно изжил наивные стремленья,<br>
&emsp;&emsp;&emsp;То наша цель теперь –<br>
&emsp;&emsp;&emsp;Лишь глупый сытый строй,<br>
Где большинство живёт одним лишь потребленьем!

Но ты была<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Сотворена<br>
&emsp;&emsp;&emsp;Как ангел жизни новой,<br>
&emsp;&emsp;&emsp;Твоя весна<br>
За собой нас вела<br>
&emsp;&emsp;&emsp;К мечтам пути земного!

И день придёт,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;И однажды тебе<br>
&emsp;&emsp;&emsp;Всё снова станет ясно;<br>
И день придёт,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;И мы скажем себе:<br>
&emsp;&emsp;&emsp;Стремленья не напрасны!
</div>