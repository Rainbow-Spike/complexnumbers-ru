---
title: В звёздном вихре времён
subtitle: ария Рассказчицы
byArtist:
- name: Елена Яковец
  role: Рассказчица
  image: /images/photos/len_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Рассказчица**

В звездном вихре времён<br>
&emsp;&emsp;&emsp;Мечты меняют цвет:<br>
Стали былью одни,<br>
&emsp;&emsp;&emsp;Других растаял след,<br>
А иные влекут на странный свет<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Уж тысячи лет,<br>
Оставаясь лишь призраком, лишь сном,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Хранящим завет.

Но чем наша цель нереальней,<br>
&emsp;&emsp;&emsp;Тем манит сильнее она,<br>
Нас верить в себя заставляя,<br>
&emsp;&emsp;&emsp;Как будто и вправду верна...

В ожиданьи мечты<br>
&emsp;&emsp;&emsp;Над пропастью во ржи<br>
Мы искали свой путь,<br>
&emsp;&emsp;&emsp;Забыв о вечной лжи,<br>
И земной этот мир<br>
&emsp;&emsp;&emsp;Был лишь для нас, и лишь от того,<br>
Что во имя мечты<br>
&emsp;&emsp;&emsp;Мы можем в миг разрушить его.

И, может быть, это так глупо:<br>
&emsp;&emsp;&emsp;В предельную цель возводить<br>
Наивную детскую сказку,<br>
&emsp;&emsp;&emsp;Но ради неё стоит жить!

В звёздном вихре времён,<br>
&emsp;&emsp;&emsp;В развитии идей<br>
Суд истории строг,<br>
&emsp;&emsp;&emsp;И нет его верней:<br>
Город Солнца земной<br>
&emsp;&emsp;&emsp;Из стен своих проектных НИИ<br>
Выйти так и не смог,<br>
&emsp;&emsp;&emsp;Оставив нам лишь краски свои…

Но чем наша цель нереальней,<br>
&emsp;&emsp;&emsp;Тем манит сильнее она,<br>
Нас светом своим ослепляя<br>
&emsp;&emsp;&emsp;В объятьях прекрасного сна,

И, может быть, это так глупо:<br>
&emsp;&emsp;&emsp;В предельную цель возводить<br>
Наивную детскую сказку,<br>
&emsp;&emsp;&emsp;Но ради неё стоит жить!
</div>
