---
title: To the Illusive Light
subtitle: Limaeva's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-12-%E2%80%93-%D0%BA-%D0%BF%D1%80%D0%B8%D0%B7%D1%80%D0%B0%D1%87%D0%BD%D0%BE%D0%BC%D1%83-%D1%81%D0%B2%D0%B5%D1%82%D1%83-2032-track-12-%E2%80%93-illu.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: Svetlana Limaeva
  image: /images/photos/ari_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

Night<br>
&emsp;&emsp;&emsp;Of the fading summer<br>
Is lit by the twilight of dawn<br>
&emsp;&emsp;&emsp;Just like if it was the shine of numerous stars<br>
I'm<br>
Flying through the images of my dreams again,<br>
In the labyrinth of reveries,<br>
&emsp;&emsp;&emsp;To the illusive light!

There,<br>
I return to that very day,<br>
To the day when I saw you for the first time<br>
&emsp;&emsp;&emsp;And suddenly realized:<br>
The whole world<br>
Just didn’t exist<br>
Before<br>
&emsp;&emsp;&emsp;That very moment.

But, right now,<br>
&emsp;&emsp;&emsp;There’s only a step between us,<br>
And I feel<br>
&emsp;&emsp;&emsp;I’m almost fainting.

That’s true,<br>
&emsp;&emsp;&emsp;I don’t know what’s meant to happen,<br>
But<br>
&emsp;&emsp;&emsp;I don’t want this very moment to end,<br>
I want it<br>
&emsp;&emsp;&emsp;To last for centuries,<br>
Just<br>
&emsp;&emsp;&emsp;In order to see that you exist in the world.

Oh, how badly<br>
&emsp;&emsp;&emsp;I wanted to meet you,<br>
Even though I knew<br>
&emsp;&emsp;&emsp;It wasn’t supposed to happen at all:<br>
You've always been<br>
On a screen only, behind those microcircuits,<br>
So far away, just like a star<br>
&emsp;&emsp;&emsp;On the Milky Way.

Back then,<br>
I had only one dream:<br>
In that dream, strangely enough,<br>
&emsp;&emsp;&emsp;We were together.<br>
That dream<br>
Completed the image of you,<br>
And the screen was turned off<br>
&emsp;&emsp;&emsp;By someone's gesture.

But this moment<br>
&emsp;&emsp;&emsp;Is nothing like that, everything’s different!<br>
That’s true, maybe,<br>
&emsp;&emsp;&emsp;I don’t mean anything to you,

But<br>
&emsp;&emsp;&emsp;The world exists only thanks to you,<br>
Illuminating the skies with light<br>
&emsp;&emsp;&emsp;In the dreams,<br>
I am<br>
&emsp;&emsp;&emsp;Just a grain of sand in eternity, but<br>
For me,<br>
&emsp;&emsp;&emsp;There’s no life<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Without you…

<br>

That’s it!<br>
&emsp;&emsp;&emsp;The time has gone,<br>
That moment is already in the past,<br>
&emsp;&emsp;&emsp;And I’ll never see that dream again.<br>
But<br>
Is there anything I can do?:<br>
You’re so far away!<br>
&emsp;&emsp;&emsp;Time knows no mercy!..
</div>
