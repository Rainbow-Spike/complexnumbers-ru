---
title: The Last Battle
subtitle: instrumental
image: /listen/2032/cover-clean.jpg
byArtist:
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

