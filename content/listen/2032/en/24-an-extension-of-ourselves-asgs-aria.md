---
title: Continuation of Self
subtitle: ASGU's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-24-%E2%80%93-%D0%BF%D1%80%D0%BE%D0%B4%D0%BE%D0%BB%D0%B6%D0%B5%D0%BD%D1%8C%D0%B5-%D1%81%D0%B5%D0%B1%D1%8F-2032-track-24-%E2%80%93-continuation.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Aleksandr Asinksy
  role: Guitar parts
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**ASGU**

We’ve hit a dead end<br>
&emsp;&emsp;&emsp;We’ve got a problem in our search for the meaning<br>
Of the inner “self”<br>
&emsp;&emsp;&emsp;Of all the sentient beings:<br>
But, just like always,<br>
&emsp;&emsp;&emsp;There are thoughts that seem so crazy,<br>
As if everything around us<br>
&emsp;&emsp;&emsp;Was just a meaningless bunch of substances.

&emsp;&emsp;&emsp;And we are going to the other extreme:<br>
If everyone in the world sees everything in the same colors,<br>
Then it seems that it’s so easy to find the life’s path for all at once<br>
&emsp;&emsp;&emsp;In the shine of their happy eyes!

We<br>
&emsp;&emsp;&emsp;Will draw our world in such a way that<br>
All<br>
&emsp;&emsp;&emsp;The people will love and perceive each other as a continuation of self<br>
There,<br>
&emsp;&emsp;&emsp;The whole world will become a single organism<br>
There<br>
&emsp;&emsp;&emsp;Will be no place for evil, envy and cynicism.

And what about life?<br>
&emsp;&emsp;&emsp;It’s following its own rules<br>
Putting things on practice is wiser<br>
&emsp;&emsp;&emsp;Than arguing about the abstract ideas<br>
And love will always be<br>
&emsp;&emsp;&emsp;Just a means of achieving happiness<br>
And happiness can be achieved<br>
&emsp;&emsp;&emsp;In hundreds of different ways!

&emsp;&emsp;&emsp;The change of formations, epochs and ideas<br>
Has paved us hundreds of ways but, just like before,<br>
In the life of every person, most people are always<br>
&emsp;&emsp;&emsp;Just decorations of the daily routines!

We<br>
&emsp;&emsp;&emsp;Are not meant to build our world in such a way that<br>
All<br>
&emsp;&emsp;&emsp;The people will love and perceive each other as a continuation of self<br>
But<br>
&emsp;&emsp;&emsp;I’m not going to cry about it:<br>
We<br>
&emsp;&emsp;&emsp;Are following the natural path!

Even though, the door to the past is already closed,<br>
&emsp;&emsp;&emsp;And there will be no turn back, believe me!<br>
Maybe, now it’s time for us to risk<br>
&emsp;&emsp;&emsp;And try to find the path we have drawn?<br>
I’m<br>
&emsp;&emsp;&emsp;Washing my hands here!
</div>