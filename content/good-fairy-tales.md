---
title: 'Сказки добрые. Каверы-переосмысления — Complex Numbers'
image: /images/covers/good-tales.jpg

album:
  title: Сказки добрые. Каверы-переосмысления (2003–2021)
  description: 11 треков разных лет, в том числе со страницами обсуждений.
  artist: Complex Numbers
  cover: /images/covers/good-tales.jpg
  credits: |
    Переработка музыки и текстов: Виктор Аргонов. Вокал: Ariel, Len, Oleviia.

  mp3: https://gitlab.com/api/v4/projects/32378455/packages/generic/release/1.0.0/2021-good-fairy-tales-mp3.zip
  vk: https://vk.com/audios-23865151?z=audio_playlist-23865151_83082488
  bandcamp: https://complexnumbers.bandcamp.com/album/good-fairy-tales
  gdrive: https://drive.google.com/drive/folders/1mLCGFycDd-XysyWogHcJX02OrIrvmcuA
  youtube: https://www.youtube.com/playlist?list=OLAK5uy_nCsWd6hkJ6wkWtGi_vSFDfh7k0tbs697Y
  apple: https://music.apple.com/album/1599085119
  amazon: https://www.amazon.com/dp/B09NBMC2H3
  spotify: https://open.spotify.com/album/5eAARap1R4ePDftK5Rnxlb
  yandex: https://music.yandex.ru/album/19865105
  jamendo: https://www.jamendo.com/album/489062/skazki-dobrye

  tracks:
  - title: Сказки добрые (В год пред вспышкой) [Каролина Trance Cover]
    description: Музыка, текст - Сергей Туманов, Виктор Аргонов, аранжировка - Виктор Аргонов, Денис Мурзин, вокал - Ariel
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/01-good-fairy-tales-karolina-trance-cover.mp3
    links:
    - text: История и обсуждение
      url: https://argonov.livejournal.com/202947.html
  - title: Прекрасное далёко [Крылатов-Энтин Trance Cover]
    description: Музыка - Евгений Крылатов, Виктор Аргонов, текст - Юрий Энтин, вокал - Олевия Кибер
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/02-bright-and-distant-future-krylatov-entin-trance-cover.mp3
  - title: Choral Prelude (Solaris Ocean) [JS Bach Trance Cover]
    description: Музыка - Johan Sebastian Bach, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/03-choral-prelude-js-bach-trance-cover.mp3
  - title: Wedding March (Post-Wagnerian World) [2021 R Wagner Nintendo & Electro Cover]
    description: Музыка - Richard Wagner, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/04-wedding-march-2021-r-wagner-nintendo-electro-cover.mp3
  - title: 8 минут (Тысяч солнц прощальный крик) [Игорь Николаев Trance Cover]
    description: Музыка, текст - Игорь Николаев, Виктор Аргонов, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/05-8-minutes-igor-nikolaev-trace-cover.mp3
    links:
    - text: История и обсуждение
      url: https://argonov.livejournal.com/228740.html
  - title: Призрачный рай [Unreal Space Cover]
    description: Музыка - Виктор Аргонов, Futurist, текст - Futurist, вокал - Len
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/06-illusive-paradise-unreal-space-cover.mp3
  - title: All the Things She Said (Post-Industrial Changes) [2021 tATu Trance Cover]
    description: Музыка - Иван Шаповалов, Сергей Галоян, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/07-all-the-things-she-said-2021-tatu-trance-cover.mp3
  - title: Wedding March [2013 R Wagner Nintendo Cover]
    description: Музыка - Richard Wagner, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/08-wedding-march-2013-r-wagner-nintendo-cover.mp3
  - title: Newtype [Unreal Trance Cover]
    description:  Музыка - Wonder, Futurist, текст - Futurist, вокал - Arashi
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/09-newtype-unreal-trance-cover.mp3
  - title: Yesterday Man [Elegant Machinery Trance Cover]
    description:  Музыка, текст - Elegant Machinery, вокал - Ariel
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/10-yesterday-man-elegant-machinery-trance-cover.mp3
  - title: All the Things She Said (Post-Industrial Changes) [2003 tATu Trance Cover]
    description:  Музыка - Иван Шаповалов, Сергей Галоян, Виктор Аргонов
    url: https://complexnumbers.gitlab.io/releases/good-fairy-tales/mp3/11-all-the-things-she-said-2003-tatu-trance-cover.mp3
---
