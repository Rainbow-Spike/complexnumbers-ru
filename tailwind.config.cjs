const colors = require('tailwindcss/colors');

const lcl = {
	50: '#ffd81c',
	100: '#ffb715',
	200: '#ff950c',
	300: '#fe6f06',
	400: '#eb5110',
	500: '#e94c0b',
	600: '#d3340a',
	700: '#bd1d08',
	800: '#a70b07',
	900: '#900',
};

const round = number =>
	number
		.toFixed(7)
		.replace(/(\.\d+?)0+$/, '$1')
		.replace(/\.0$/, '');
const rem = px => `${round(px / 16)}rem`;

module.exports = {
	content: [
		'./src/**/*.{html,js,svelte,ts}',
	],
	theme: {
		fontFamily: {
			sans: ['InterVariable', 'sans-serif'],
		},
		extend: {
			colors: {
				gray: colors.stone,
				lcl,
				dark: '#0d0d0d',
			},

			screens: {
				// "hover" disabler for mobiles
				// https://stackoverflow.com/a/60478751/337979
				hoverable: {raw: '(hover: hover)'},

				// For small windows resized in vertical direction
				vnarrow: {raw: '(max-height: 600px)'},
			},

			listStyleType: {
				square: 'square',
			},

			transitionProperty: {
				width: 'width',
			},

			boxShadow: {
				sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
				DEFAULT: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
				md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
				lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
				xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
				'2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
				'3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
				inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
				none: 'none',
				'lcl-900': '0 0 60px #760808;',
			},

			dropShadow: {
				basic: '1px 1px 3px #00000070',
				bigger: '1px 1px 10px #00000070',
			},

			backgroundImage: () => ({
				2032: 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAACAElEQVQYGQXB23bbRgwF0HMAzFCkpFhu3ZWkTla+LP/ep744jiyRFC8zA2Rv+/n6XYVJqETzVj2UMqh2QkYADkQFN8fuCMBMREgVJoEJpDlAIiKCCAIqIiSEGiEMe8oiFBAqYEgRjwgLmEBAI0yV5AEIhCns66CgNtIjwiOgRAgpACOUyComQgkGRGCvJ22QEiwOB5TISiMQ3lqQkUSTUAUMOGAvZ6wFW9CDRh4yhsxsaM5tLaWAjCw0gmAQ1h8dq0iDCI+Zl7Oen5B6tIbxyvnafA8NmFAzUxI7XUIX1J3J5Oksz5/l+K/q39Za5P81/bcsbxWbJ9V+QHeE9Z9ckyPkMPD0IsfvKf848J+DhA6f19bfopviXjJL11frxbZ1r9GGYxq+6OHbga+9fz3JpSfVcu4dsPBfo9XdxENg49sSKiq5o6X+xL6DdvBMQtTSU4ovuaXMuUZpbXerc4VgqrWY7Lk7SzpUSyeoIKalXZc6lu3hZUZdWFaar60hppmxjNMjtvv+6XU9/jWYsI3r+v6Y37f7db9P7bF42d3K7o8W9xIxt8fq81yer/vlpe9M21SX3/v4Ud4//NfYbluUFraUdi/xUaBE8bhtfrvX58vSpewb1ymmh/9+xNsStz32Fra1mBvmFr2Au897mRafPyqtbjWtlUvj2OJWMJfYK/4AaTJLmrVyzzAAAAAASUVORK5CYII=")',
				banner: 'url(/images/lelogo-clean.webp), url(/images/lelogo-clean.jpg)',
			}),

			typography: {
				DEFAULT: {
					css: {
						color: colors.neutral[200],
						a: {
							color: null,
							fontWeight: null,
							textDecoration: null,
						},
						'a:hover': {
							color: null,
						},
						strong: {
							color: null,
						},
						h1: {
							color: null,
							fontWeight: null,
							fontSize: null,
						},
						h2: {
							color: null,
							fontWeight: null,
							fontSize: null,
						},
						h3: {
							color: null,
							fontWeight: null,
							fontSize: null,
							textTransform: null,
							marginTop: null,
							marginBottom: null,
						},
						h4: {
							color: null,
							fontSize: null,
						},
						'ol > li::marker': {
							color: null,
						},
						'ul > li::marker': {
							color: null,
						},
						ul: {
							listStyleType: 'square',
						},
						blockquote: {
							color: null,
						},
						code: {
							color: null,
						},
						'code::before': false,
						'code::after': false,
						'a code': {
							color: null,
						},
					},
				},
				sm: {
					css: [{
						fontSize: rem(14),
						lineHeight: round(24 / 14),
						h1: {
							fontSize: null,
							lineHeight: null,
							marginTop: null,
							marginBottom: null,
						},
						h2: {
							fontSize: null,
							lineHeight: null,
							marginTop: null,
							marginBottom: null,
						},
						h3: {
							fontSize: null,
							lineHeight: null,
							marginTop: null,
							marginBottom: null,
						},
						h4: {
							fontSize: null,
							lineHeight: null,
							marginTop: null,
							marginBottom: null,
						},
						table: {
							fontSize: null,
						},
					}],
				},
			},
		},
	},
	plugins: [
		require('@tailwindcss/typography')({
			modifiers: ['sm'],
		}),
		require('tailwindcss-opentype'),
	],
};
