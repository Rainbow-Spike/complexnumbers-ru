/* eslint-disable capitalized-comments */
// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	// interface Locals {}
	// interface Platform {}
	// interface PrivateEnv {}
	// interface PublicEnv {}
	// interface Session {}
	// interface Stuff {}
}

declare namespace svelte.JSX {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	interface HTMLAttributes<T> {
		onjump?: (event: CustomEvent) => void;
	}
}
