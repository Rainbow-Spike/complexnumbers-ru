import {redirect} from '@sveltejs/kit';

import type {PageLoad} from './$types';

export const load: PageLoad = ({params: {album}}) => {
	// eslint-disable-next-line @typescript-eslint/no-throw-literal
	throw redirect(302, `/listen/${album}/ru/`);
};
