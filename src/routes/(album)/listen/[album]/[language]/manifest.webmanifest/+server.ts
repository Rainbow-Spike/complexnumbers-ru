import {json} from '@sveltejs/kit';
import type {RequestHandler} from './$types';
import {loadIndexFile, loadIndexMdMetadata} from '$lib/AudioPlayer/utils';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const GET: RequestHandler = ({params}) => {
	const {album, language} = params;

	const albumIndex = loadIndexFile(album);
	const indexMetadata = loadIndexMdMetadata(album, language);

	// Manifest must use snake case, disabling eslint rule
	/* eslint-disable @typescript-eslint/naming-convention */
	const manifest = {
		short_name: indexMetadata.title,
		name: indexMetadata.title,
		start_url: `/listen/${album}/${language}/`,
		icons: albumIndex.manifestIcons,
		background_color: albumIndex.themeColor,
		display: 'standalone',
		scope: `/listen/${album}/${language}/`,
		theme_color: albumIndex.themeColor,
		description: indexMetadata.meta.description,
		categories: ['music'],
	};
		/* eslint-enable @typescript-eslint/naming-convention */

	return json(manifest);
};
