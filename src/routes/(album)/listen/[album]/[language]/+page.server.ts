import type {PageServerLoad} from './$types';
import {processIndex} from './[track]/+page.server';

export const load: PageServerLoad = async ({params: {album, language}}) =>
	processIndex(album, language);
