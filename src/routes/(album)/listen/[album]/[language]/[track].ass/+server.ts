import {readSync} from 'to-vfile';
import type {RequestHandler} from './$types';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const GET: RequestHandler = ({params: {album, language, track}}) => {
	const subtitlesFilename = `content/listen/${album}/${language}/${track}.ass`;
	let subtitlesFile: ReturnType<typeof readSync>;
	try {
		subtitlesFile = readSync(subtitlesFilename, 'utf8');

		return new Response(subtitlesFile.value, {
			headers: {
				'Content-Type': 'text/plain',
			},
		});
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code !== 'ENOENT') {
			throw error_;
		}
	}
};
