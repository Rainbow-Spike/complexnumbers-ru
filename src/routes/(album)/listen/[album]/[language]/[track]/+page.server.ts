
import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import frontmatter from 'remark-frontmatter';
import yaml from 'js-yaml';
import textr from 'remark-textr';
import {error} from '@sveltejs/kit';
import type {PageServerLoad} from './$types';
import type {IndexPageMetadata, TrackPageMetadata} from '$lib/AudioPlayer/types';
import type {Root as SubsrtRoot} from '$lib/unified/subast';
import rehypeMergeSubtitles, {type MdastWithSubtitles} from '$lib/unified/rehype-merge-subtitles';
import subsrtParse from '$lib/unified/subsrt-parse';
import subastGetTimings from '$lib/unified/subast-get-timings';

function ellipses(input: string) {
	return input.replace(/\.{3}/gim, '…');
}

const subsrtParser = unified()
	.use(subsrtParse, {format: 'ass'});

const remarkParser = unified()
	.use(remarkParse) // Parse Markdown to mdast syntax tree
	.use(frontmatter, ['yaml']); // Split front matter block

const runner = unified()
	.use(rehypeMergeSubtitles)
	.use(textr, {plugins: [ellipses]})
	.use(remark2rehype, {allowDangerousHtml: true}) // Mutate to rehype
	.use(rehypeRaw) // Deal with HTML in Markdown
	.use(rehypeStringify); // Stringify hast syntax tree to HTML

export const processTrack = async (album: string, language: string, track: string) => {
	// Parse subtitles
	const subtitlesFilename = `content/listen/${album}/${language}/${track}.ass`;
	let subtitlesFile: ReturnType<typeof readSync>;
	try {
		subtitlesFile = readSync(subtitlesFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code !== 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(500, String(error_));
		}
	}

	let timings: Array<[number, number]>;
	let subtitles: SubsrtRoot;

	if (subtitlesFile) {
		subtitles = subsrtParser.parse(subtitlesFile.value) as SubsrtRoot;
		timings = subastGetTimings(subtitles) as Array<[number, number]>;
	}

	// Parse yaml from markdown file
	const lyricsFilename = `content/listen/${album}/${language}/${track}.md`;
	let lyricsFile: ReturnType<typeof readSync>;
	try {
		lyricsFile = readSync(lyricsFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code === 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(404);
		}

		// eslint-disable-next-line @typescript-eslint/no-throw-literal
		throw error(500, String(error_));
	}

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(lyricsFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as TrackPageMetadata;
	tree.children.shift();

	(tree as MdastWithSubtitles).subtitles = subtitles;

	// Generate html from markdown
	const content: string = runner.stringify(await runner.run(tree));

	return {metadata, content, timings};
};

export const processIndex = async (album: string, language: string) => {
	// Parse yaml from markdown file
	const lyricsFilename = `content/listen/${album}/${language}/index.md`;
	let lyricsFile: ReturnType<typeof readSync>;
	try {
		lyricsFile = readSync(lyricsFilename, 'utf8');
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code === 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(404);
		}

		// eslint-disable-next-line @typescript-eslint/no-throw-literal
		throw error(500, String(error_));
	}

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(lyricsFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as IndexPageMetadata;
	tree.children.shift();

	// Generate html from markdown
	const content: string = runner.stringify(await runner.run(tree));

	return {metadata, content};
};

export const load: PageServerLoad = async ({params: {album, language, track}, url}) => {
	const page = await processTrack(album, language, track);
	return {
		...page,
		key: url.pathname,
	};
};
