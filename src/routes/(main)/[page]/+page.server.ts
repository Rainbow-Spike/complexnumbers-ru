
import type {PageServerLoad} from './$types';
import {process} from '$lib/markdown';

export const load: PageServerLoad = ({params: {page}}) =>
	process(`content/${page}.md`);
