
import type {PageServerLoad} from './$types';
import {process} from '$lib/markdown';

export const load: PageServerLoad = ({params: {page, language}}) =>
	process(`content/${page}.${language}.md`, language);
