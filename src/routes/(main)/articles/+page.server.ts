import {readSync} from 'to-vfile';
import {matter} from 'vfile-matter';
import glob from 'glob';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import rehypeExternalLinks from 'rehype-external-links';
import type {PageServerLoad} from './$types';
import {rehypePrefetchLinks} from '$lib/unified/rehype-prefetch-links';
import {rehypeAddLinksClasses} from '$lib/unified/rehype-add-link-classes';
import {typografs} from '$lib/markdown';

const remarkParser = unified()
	.use(remarkParse)
	.use(remarkRehype, {allowDangerousHtml: true})
	.use(rehypeRaw)
	.use(rehypePrefetchLinks)
	.use(rehypeAddLinksClasses)
	.use(rehypeExternalLinks, {target: '_blank', rel: ['external', 'noopener', 'noreferrer']})
	.use(rehypeStringify);

const extractFrontmatter = (path: string) => {
	const file = readSync(path);
	const name = /^content\/articles\/(.+)\.md$/.exec(path)[1];

	const front = matter(file, {strip: true});
	const metadata = front.data.matter as Record<string, any>;

	metadata.link = `/articles/${name}/`;

	if (typeof metadata.title === 'string') {
		metadata.title = typografs.ru.execute(metadata.title);
	}

	if (typeof metadata.summary === 'string') {
		metadata.summary = typografs.ru.execute(
			String(remarkParser.processSync(metadata.summary).value),
		);
	}

	if (typeof metadata.date !== 'string') {
		metadata.date = String(metadata.date);
	}

	return metadata;
};

export const load: PageServerLoad = () => {
	const fileNames = glob.sync('content/articles/*.md');
	const articles = fileNames.map(path => extractFrontmatter(path));
	articles.sort((a, b) => {
		const keyA = new Date(a.date);
		const keyB = new Date(b.date);
		if (keyA < keyB) {
			return 1;
		}

		if (keyA > keyB) {
			return -1;
		}

		return 0;
	});

	return {articles};
};
