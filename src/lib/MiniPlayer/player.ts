import {get} from 'svelte/store';
import {
	activeIndex as activeIndexStore, playlist as playlistStore, playPosition as playPositionStore,
	playStatus as playStatusStore, volume as volumeStore, autoplay as autoplayStore,
	bufferedTo as bufferedToStore, repeatTrack as repeatTrackStore,
	PlayStatus, duration as durationStore,
} from './stores';
import {browser} from '$app/env';

export enum SkipDirection {
	Previous, Next,
}

declare global {
	interface Window {
		miniPlayer: Player;
		webkitAudioContext: typeof AudioContext;
	}
}

export class Player {
	audio: HTMLAudioElement;
	nextAudio: HTMLAudioElement;

	sourceNode: MediaElementAudioSourceNode;
	nextSourceNode: MediaElementAudioSourceNode;
	audioContext: AudioContext;
	skipMediaSessionSetup: boolean;

	constructor(
	) {
		if (browser) {
			this.audio = this._createAudio('channel2');
			this.nextAudio = this._createAudio('channel1');
			this.skipMediaSessionSetup = !('mediaSession' in navigator);
		}
	}

	_createAudio(channel: string) {
		const audio = new Audio();
		audio.dataset.channel = channel;
		audio.crossOrigin = 'anonymous';
		audio.volume = this.volume;
		volumeStore.subscribe(newVolume => {
			audio.volume = newVolume;
		});

		audio.addEventListener('loadedmetadata', () => {
			durationStore.set(audio.duration);
			console.log(audio.dataset.channel, 'onloadedmetadata', this.audio.duration);
		});

		audio.addEventListener('canplaythrough', () => {
			if (!this.currentSong) {
				console.log(audio.dataset.channel, 'no current song!!');
				return;
			}

			console.log(audio.dataset.channel, 'oncanplaythrough');
			this.bufferedTo = this.audio.duration;
		});

		audio.addEventListener('ended', () => {
			console.log(audio.dataset.channel, 'onended');

			if (!this.autoplay) {
				return;
			}

			if (this.repeatTrack) {
				this.seek(0);
				this.play();
			} else {
				this.playStatus = PlayStatus.Ended;
				this._setMediaSessionPlaybackState('none');
				this.skip(SkipDirection.Next);
			}
		});

		audio.addEventListener('load', () => {
			console.log(audio.dataset.channel, 'onload');
		});

		audio.addEventListener('pause', () => {
			console.log(audio.dataset.channel, 'onpause');
			this.playStatus = PlayStatus.Paused;
			this._setMediaSessionPlaybackState('paused');
		});

		audio.addEventListener('timeupdate', () => {
			this.playPosition = this.audio.currentTime;
		});

		audio.addEventListener('progress', () => {
			const audio = this.audio;
			for (let i = 0; i < audio.buffered.length; i++) {
				if (audio.buffered.start(audio.buffered.length - 1 - i) < audio.currentTime) {
					this.bufferedTo = audio.buffered.end(audio.buffered.length - 1 - i);
					break;
				}
			}
		});

		audio.addEventListener('waiting', _event => {
			console.log(audio.dataset.channel, 'onwaiting');
			this.playStatus = PlayStatus.Waiting;
		});

		audio.addEventListener('seeked', () => {
			console.log(audio.dataset.channel, 'onseeked');
		});

		audio.addEventListener('playing', () => {
			if (!this.currentSong) {
				return;
			}

			console.log(audio.dataset.channel, 'onplaying');

			this.playStatus = PlayStatus.Playing;
			this.updatePositionState();
		});

		audio.addEventListener('play', () => {
			console.log(audio.dataset.channel, 'onplay');
			this._setMediaSessionPlaybackState('playing');
		});

		return audio;
	}

	_setupMediaSessionHandlers() {
		if (this.skipMediaSessionSetup) {
			return;
		}

		const actions: {[action in MediaSessionAction]?: MediaSessionActionHandler} = {
			async play() {
				console.log('MediaSession play!');
				await player.audio.play();
			},
			pause() {
				console.log('MediaSession pause!');
				player.audio.pause();
			},
			stop() {
				console.log('MediaSession stop!');
				player.stop();
			},
			seekbackward({seekOffset}) {
				console.log('MediaSession seekbackward!');
				player.seek(player.playPosition - (seekOffset || 5));
			},
			seekforward({seekOffset}) {
				console.log('MediaSession seekforward!');
				player.seek(player.playPosition + (seekOffset || 5));
			},
			seekto(event) {
				console.log('MediaSession seekto!');
				player.seek(event.seekTime, event.fastSeek);
			},
			previoustrack() {
				console.log('MediaSession previoustrack!');
				player.skip(SkipDirection.Previous);
			},
			nexttrack() {
				console.log('MediaSession nexttrack!');
				player.skip(SkipDirection.Next);
			},
		};

		for (const [action, handler] of Object.entries(actions)) {
			try {
				navigator.mediaSession.setActionHandler(action as MediaSessionAction, handler);
			} catch {
				console.warn(`The media session action ${action} is not supported:`);
			}
		}

		this.skipMediaSessionSetup = true;
	}

	// Shortcut accessors for stores
	get index() {
		return get(activeIndexStore);
	}

	set index(value: number) {
		activeIndexStore.set(value);
	}

	get playPosition() {
		return get(playPositionStore);
	}

	set playPosition(value: number) {
		playPositionStore.set(value);
	}

	get bufferedTo() {
		return get(bufferedToStore);
	}

	set bufferedTo(value: number) {
		bufferedToStore.set(value);
	}

	get tracks() {
		return get(playlistStore).tracks;
	}

	get album() {
		return get(playlistStore);
	}

	get playStatus() {
		return get(playStatusStore);
	}

	set playStatus(value: PlayStatus) {
		playStatusStore.set(value);
	}

	get volume() {
		return get(volumeStore);
	}

	get repeatTrack() {
		return get(repeatTrackStore);
	}

	get autoplay() {
		return get(autoplayStore);
	}

	get duration() {
		return get(durationStore);
	}

	get currentSong() {
		return this.tracks[this.index];
	}

	_setMediaSessionMetadata = (metadata: MediaMetadataInit) => {
		if ('mediaSession' in navigator) {
			navigator.mediaSession.metadata = new MediaMetadata(metadata);
		}
	};

	_setMediaSessionPlaybackState = (state: MediaSessionPlaybackState) => {
		if ('mediaSession' in navigator) {
			navigator.mediaSession.playbackState = state;
		}
	};

	_setMediaSessionPositionState = (state: MediaPositionState) => {
		if ('mediaSession' in navigator && 'setPositionState' in navigator.mediaSession) {
			navigator.mediaSession.setPositionState(state);
		}
	};

	updatePositionState() {
		console.log('updatePositionState', {
			duration: this.audio.duration,
			playbackRate: this.audio.playbackRate,
			position: this.audio.currentTime,
		});

		this._setMediaSessionPositionState({
			duration: this.audio.duration,
			playbackRate: this.audio.playbackRate,
			position: this.audio.currentTime,
		});
	}

	updateMetadata() {
		const album = this.album;
		const type = album.cover.endsWith('.png') ? 'image/png' : 'image/jpеg';
		this._setMediaSessionMetadata({
			title: this.currentSong.title,
			artist: album.artist,
			album: album.title,
			artwork: [
				{src: album.cover, sizes: '1000x1000', type},
			],
		});
	}

	prefetch(index: number) {
		if (index === null) {
			console.log('nothing to prefetch!!');
			return;
		}

		const audioFile = this.tracks[index].url;

		if (this.nextAudio.src !== audioFile) {
			console.log(`Fetch audio from ${audioFile} into ${this.nextAudio.dataset.channel}`);
			this.nextAudio.src = audioFile;
		}
	}

	loadTrack(index: number) {
		if (index === null) {
			console.log('nothing to load!!');
			return;
		}

		this.index = index;

		const audioFile = this.currentSong.url;

		if (this.nextAudio.src !== audioFile) {
			if (this.nextAudio.src) {
				console.log(`Using non-prefetched file, prefetched file was ${this.nextAudio.src}, but needed ${audioFile}`);
			}

			this.prefetch(index);
		}

		// Swap active audio with prefetched one
		[this.nextAudio, this.audio] = [this.audio, this.nextAudio];
		[this.nextSourceNode, this.sourceNode] = [this.sourceNode, this.nextSourceNode];

		this.nextAudio.src = '';

		this.bufferedTo = 0;
	}

	play() {
		this._setupMediaSessionHandlers();

		console.log('play');

		void this.audio.play().then(() => {
			this.updateMetadata();
			this.updatePositionState();
		});
	}

	pause() {
		this.audio.pause();
	}

	stop() {
		this.audio.pause();
		this.audio.currentTime = 0;
		this.playStatus = PlayStatus.Stopped;
		console.log(this.audio.dataset.channel, 'stopped');
	}

	skip(direction: SkipDirection) {
		let index = 0;
		if (direction === SkipDirection.Previous) {
			index = this.index - 1;
			if (index < 0) {
				index = this.tracks.length - 1;
			}
		} else {
			index = this.index + 1;
			if (index >= this.tracks.length) {
				index = 0;
			}
		}

		this.loadTrack(index);
		this.play();
	}

	seek(seekTime: number, fastSeek = false) {
		const audio = this.audio;
		console.log(audio.dataset.channel, `seek ${seekTime} / ${this.audio.duration}`);
		seekTime = Math.max(0, seekTime);

		if (fastSeek && 'fastSeek' in this.audio) {
			audio.fastSeek(seekTime);
			return;
		}

		audio.currentTime = seekTime;
		this.playPosition = seekTime;
		console.log(audio.dataset.channel, 'after seek', audio.currentTime);

		this.updatePositionState();

		if (audio.currentTime === 0 && seekTime !== 0) {
			console.log('!!! <seeking failed, should never happen!> !!!');
		}
	}
}

export const player: Player = browser ? window.miniPlayer || new Player() : null;

if (browser) {
	window.miniPlayer = player;
}
