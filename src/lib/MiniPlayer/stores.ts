import {derived, writable} from 'svelte/store';
import type {AlbumMetadata} from './types';
import {browser} from '$app/env';

// List of tracks from current page
export const album = writable(null as AlbumMetadata);

// List of tracks loaded into player
export const playlist = writable(null as AlbumMetadata);

export const activeIndex = writable(null as number);

export const activeTrack = derived(
	[playlist, activeIndex],
	([$playlist, $activeIndex]) => {
		if ($playlist === null || $activeIndex === null) {
			return null;
		}

		return $playlist.tracks[$activeIndex];
	},
);

export enum PlayStatus {
	None, Stopped, Playing, Waiting, Paused, Ended,
}
export const playStatus = writable(PlayStatus.None);

export const playPosition = writable(0);
export const bufferedTo = writable(0);

export const duration = writable(0);

function createVolumeStore() {
	let storedVolume = 1;
	if (browser) {
		const lsVolume = localStorage.getItem('ap_volume');
		if (lsVolume !== null) {
			storedVolume = Number(storedVolume);
		}
	}

	const {subscribe, set} = writable(storedVolume);

	if (browser) {
		subscribe(value => {
			localStorage.setItem('ap_volume', value.toString());
		});
	}

	return {
		subscribe,
		set(value: number) {
			set(Math.max(0, Math.min(1, value)));
		},
	};
}

export const volume = createVolumeStore();

export const autoplay = writable(true);

export const repeatTrack = writable(false);
