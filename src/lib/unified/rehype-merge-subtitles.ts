import type {Plugin} from 'unified';
import type {Root as MdastRoot, Text} from 'mdast';
import type {Node} from 'unist';
import {is} from 'unist-util-is';
import {u} from 'unist-builder';
import flatMap from 'unist-util-flatmap';
import type {Caption, Root as SubsrtRoot} from './subast';

export interface MdastWithSubtitles extends MdastRoot {
	subtitles: SubsrtRoot;
}

const split = (input: string, separator: string) => {
	const idx = input.indexOf(separator);
	if (idx === -1) {
		throw new Error('separator not found');
	}

	return [input.slice(0, idx), input.slice(idx + separator.length)];
};

const rehypeMergeSubtitles: Plugin = function () {
	return function (tree: MdastWithSubtitles) {
		if (!tree.subtitles) {
			return;
		}

		const captions = tree.subtitles.children
			.filter(node => is<Caption>(node, 'caption'))
			.map(({text, start, end}: Caption) => ({text, start, end}));

		let capIdx = 0;

		flatMap(tree, (node: Node) => {
			if (captions.length === 0) {
				return [node];
			}

			if (is<Text>(node, 'text')) {
				let value = node.value;
				const newNodes: Node[] = [];
				while (captions.length > 0 && value.includes(captions[0].text)) {
					const [head, tail] = split(value, captions[0].text);
					if (head.length > 0) {
						newNodes.push(u('text', head));
					}

					newNodes.push(
						u('html', `<span data-line-id="${capIdx}" class="captions-line">`),
						u('text', captions[0].text),
						u('html', '</span>'));
					value = tail;
					captions.shift();
					capIdx++;
				}

				if (newNodes.length > 0 && value.length > 0) {
					newNodes.push(u('text', value));
				}

				return newNodes.length > 0 ? newNodes : [node];
			}

			return [node];
		});

		if (captions.length > 0) {
			const error = `Unable to find caption line "${captions[0].text}" in markdown file`;
			console.error(error);
			throw new Error(error);
		}
	};
};

export default rehypeMergeSubtitles;
