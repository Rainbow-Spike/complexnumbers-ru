import type {Plugin} from 'unified';
import {visit} from 'unist-util-visit';
import type {Root, Element} from 'hast';

const absoluteRe = /^(?:[a-z]+:)?\/\//i;

export const rehypePrefetchLinks: Plugin = () => (tree: Root) => {
	visit(tree, 'element', (node: Element) => {
		if (
			node.tagName === 'a'
            && node.properties
            && typeof node.properties.href === 'string'
		) {
			const url = node.properties.href;

			if (!absoluteRe.test(url)) {
				node.properties['sveltekit:prefetch'] = '';
			}
		}
	});
};
