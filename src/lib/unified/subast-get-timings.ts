
import {is} from 'unist-util-is';
import type {Caption, Root as SubsrtRoot} from './subast';

const subastGetTimings = function (subtitles: SubsrtRoot) {
	return subtitles.children
		.filter(node => is<Caption>(node, 'caption'))
		.map(({start, end}: Caption) => [start, end]);
};

export default subastGetTimings;
