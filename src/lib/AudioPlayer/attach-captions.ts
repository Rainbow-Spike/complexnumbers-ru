import type {Readable} from 'svelte/store';
import IntervalTree from '@flatten-js/interval-tree';
import scrollToFit from '$lib/scroll-to-fit';
import {browser} from '$app/env';

export const buildIntervalTree = function (timings?: Array<[number, number]>) {
	let intervalTree = null as IntervalTree<number>;

	if (browser) {
		if (timings) {
			intervalTree = new IntervalTree();
			for (const [i, timing] of timings.entries()) {
				intervalTree.insert(timing, i);
			}
		} else {
			intervalTree = null;
		}
	}

	return intervalTree;
};

type AttachCaptionsOptions = {timings: Array<[number, number]>; positionStore: Readable<number>};

const attachCaptions = (node: HTMLElement, options: AttachCaptionsOptions) => {
	let captionElements: NodeListOf<HTMLElement>;
	let sortedCaptionElements: HTMLElement[];
	let intervalTree: IntervalTree<number>;
	let activeCaptions: HTMLElement[];

	const setupHandlers = (timings: Array<[number, number]>) => {
		if (!options.timings) {
			intervalTree = undefined;
			activeCaptions = undefined;
			return;
		}

		activeCaptions = [];

		intervalTree = buildIntervalTree(timings);

		const onclick = (event_: MouseEvent) => {
			const target = event_.target as HTMLElement;
			const lineId = Number(target.dataset.lineId);
			const start = timings[lineId][0];
			node.dispatchEvent(new CustomEvent('jump', {detail: start / 1000}));
		};

		captionElements = node.querySelectorAll('span[data-line-id]');

		sortedCaptionElements = [];
		for (const element of captionElements) {
			sortedCaptionElements[Number(element.dataset.lineId)] = element;
		}

		for (const element of captionElements) {
			element.addEventListener('click', onclick);
		}

		node.scrollTo({top: 0, behavior: 'auto'});
	};

	setupHandlers(options.timings);

	const followPlayPosition = (pos: number) => {
		if (!intervalTree) {
			return;
		}

		const transitionDuration = 200;
		const newActiveCaptionIds = intervalTree.search([
			pos * 1000 + transitionDuration / 2,
			pos * 1000 + transitionDuration / 2,
		]) as number[];
		const newActiveCaptions = newActiveCaptionIds.map(i => sortedCaptionElements[i]);

		const same = newActiveCaptions.length === activeCaptions.length
            && newActiveCaptions.every((element, index) => element === activeCaptions[index]);

		if (!same) {
			for (const captionElement of activeCaptions) {
				captionElement.classList.remove('captions-line-active');
			}

			for (const captionElement of newActiveCaptions) {
				captionElement.classList.add('captions-line-active');
			}

			if (newActiveCaptions.length > 0) {
				scrollToFit({elements: newActiveCaptions, container: node});
			}

			activeCaptions = newActiveCaptions;
		}
	};

	const unsub = options.positionStore.subscribe(followPlayPosition);

	return {
		update(options: AttachCaptionsOptions) {
			setupHandlers(options.timings);
		},

		destroy() {
			unsub();
		},
	};
};

export default attachCaptions;
