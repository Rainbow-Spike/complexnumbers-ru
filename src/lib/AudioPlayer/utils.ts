import yaml from 'js-yaml';
import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import remarkFrontmatter from 'remark-frontmatter';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import {error} from '@sveltejs/kit';
import type {AlbumIndexYaml, AlbumMetadata, IndexPageMetadata, TrackPageMetadata} from './types';
import {formatAlbumPageTitle, formatTrackPageTitle} from './formatters';

export const loadIndexFile = (album: string) => {
	try {
		const filename = `content/listen/${album}/index.yaml`;
		const file = readSync(filename, 'utf8');
		const yamlContent = yaml.load(file.toString()) as AlbumIndexYaml;
		return yamlContent;
	} catch (error_: unknown) {
		if ((error_ as {code: string}).code === 'ENOENT') {
			// eslint-disable-next-line @typescript-eslint/no-throw-literal
			throw error(404);
		}

		throw error_;
	}
};

const remarkParser = unified()
	.use(remarkParse) // Parse Markdown to mdast syntax tree
	.use(remarkFrontmatter, ['yaml']); // Split front matter block

export const loadMarkdownMetadata = function (filename: string) {
	const file = readSync(filename, 'utf8');
	const tree = remarkParser.parse(file);
	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as Record<string, unknown>;
	return metadata;
};

export const loadIndexMdMetadata = function (album: string, language: string) {
	const filename = `content/listen/${album}/${language}/index.md`;
	return loadMarkdownMetadata(filename) as IndexPageMetadata;
};

type TrackMdMetadata = {
	title: string;
	subtitle?: string;
	image?: string;
	translation?: string;
	translator?: string;
	translators?: string;
};

export const loadTrackMdMetadata = function (album: string, language: string, track: string) {
	const filename = `content/listen/${album}/${language}/${track}.md`;
	return loadMarkdownMetadata(filename) as TrackMdMetadata;
};

const runner = unified()
	.use(remark2rehype, {allowDangerousHtml: true}) // Mutate to rehype
	.use(rehypeRaw) // Deal with HTML in Markdown
	.use(rehypeStringify); // Stringify hast syntax tree to HTML

export const loadIndexMd = (album: string, language: string) => {
	// Parse yaml from markdown file
	const indexFilename = `content/listen/${album}/${language}/index.md`;
	const indexFile = readSync(indexFilename, 'utf8');

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(indexFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as IndexPageMetadata;
	tree.children.shift();

	// Generate html from markdown
	const content: string = runner.stringify(runner.runSync(tree));

	return {metadata, content};
};

export const getTitle = (
	album: AlbumMetadata,
	trackPageMetadata: TrackPageMetadata,
) =>
	trackPageMetadata
		? formatTrackPageTitle(album, trackPageMetadata)
		: formatAlbumPageTitle(album);

export const getCanonicalUrl = (url: URL) => (
	'https://complexnumbers.ru/'
		+ url.pathname
		+ (url.pathname.endsWith('/') ? '' : '/')
);
