import type {TrackPageMetadata} from './types';

export const showGallery = ({byArtist}: TrackPageMetadata) => (
	byArtist.map(element => Number(Boolean(element.image))).reduce((a, b) => a + b) > 1
);

export const showGalleryExtra = ({byArtist, translator}: TrackPageMetadata) => translator || (
	byArtist.map(element => Number(!element.image)).reduce((a, b) => a + b) > 0
);

export const getLinkName = (url: string) => {
	const matches = /^https?:\/\/([^/?#]+)(?:[/?#]|$)/i.exec(url);
	const domain = matches?.[1];
	return domain ?? url;
};
