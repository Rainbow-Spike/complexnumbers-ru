import {AlbumMetadata} from './MiniPlayer/types';

export type MarkdownContentPageMetadata = {
	title: string;
	description?: string;
	summary?: string;
	date?: string;
	image?: string;
	twitterTitle?: string;
	twitterDescription?: string;
	schema?: Record<string, any>;
	album?: AlbumMetadata;
};
