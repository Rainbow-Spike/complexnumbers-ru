import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import gfm from 'remark-gfm';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeExternalLinks from 'rehype-external-links';
import rehypeRaw from 'rehype-raw';
import frontmatter from 'remark-frontmatter';
import slug from 'remark-slug';
import yaml from 'js-yaml';
import Typograf from 'typograf';
import {error} from '@sveltejs/kit';
import type {MarkdownContentPageMetadata} from './types';
import {rehypeAddLinksClasses} from '$lib/unified/rehype-add-link-classes';
import {rehypePrefetchLinks} from '$lib/unified/rehype-prefetch-links';

export const typografs: Record<string, typograf.Typograf> = {
	ru: new Typograf({locale: ['ru']}),
	en: new Typograf({locale: ['en-US']}),
};

// Fill list of rules: https://github.com/typograf/typograf/blob/dev/docs/RULES.ru.md

for (const typograf of Object.values(typografs)) {
	// White quotes around links look bad
	typograf.disableRule('common/punctuation/quoteLink');
	// Disabled for 0x12345 (e. g. wallets)
	typograf.disableRule('common/number/times');
	// Disabled for PayPal
	typograf.disableRule('common/html/e-mail');
}

const parser = unified()
	.use(remarkParse)
	.use(gfm)
	.use(frontmatter, ['yaml']);

const runner = unified()
	.use(slug)
	.use(remark2rehype, {allowDangerousHtml: true})
	.use(rehypeRaw)
	.use(rehypePrefetchLinks)
	.use(rehypeAddLinksClasses)
	.use(rehypeExternalLinks, {target: '_blank', rel: ['external', 'noopener', 'noreferrer']})
	.use(rehypeStringify);

export const process = (filename: string, language = 'ru') => {
	const typograf = typografs[language];

	let file: ReturnType<typeof readSync>;
	try {
		file = readSync(filename, 'utf8');
	} catch (error_: unknown) {
		// eslint-disable-next-line @typescript-eslint/no-throw-literal
		throw (error_ as {code: string}).code === 'ENOENT' ? error(404) : error_;
	}

	const tree = parser.parse(file);

	let metadata: MarkdownContentPageMetadata;
	if (tree.children.length > 0 && tree.children[0].type === 'yaml') {
		metadata = yaml.load(tree.children[0].value) as MarkdownContentPageMetadata;
		tree.children = tree.children.slice(1, tree.children.length);
	}

	let content = runner.stringify(runner.runSync(tree));

	if (typograf) {
		content = typograf.execute(content);
	}

	return {metadata, content};
};
