declare module 'unist-util-flatmap' {
	import type {Parent, Node} from 'unist';

	export default function flatMap(root: Parent, cb: (node: Node) => Node[]);
}
