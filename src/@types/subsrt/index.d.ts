declare module 'subsrt' {
	export as namespace subsrt;

	type SupportedFormats = 'sub' | 'srt' | 'sbv' | 'vtt' | 'lrc' | 'smi' | 'ssa' | 'ass' | 'json';

	type SimpleFormat = {
		build: any;
		detect: any;
		name: string;
		parse: any;
	};

	type TimeHelperFormat = SimpleFormat & {
		helper: {
			toMilliseconds: any;
			toTimeString: any;
		};
	};

	type HtmlHelperFormat = SimpleFormat & {
		helper: {
			htmlDecode: any;
			htmlEncode: any;
		};
	};

	export const format: {
		ass: TimeHelperFormat;
		json: SimpleFormat;
		lrc: TimeHelperFormat;
		sbv: TimeHelperFormat;
		smi: HtmlHelperFormat;
		srt: TimeHelperFormat;
		ssa: TimeHelperFormat;
		sub: SimpleFormat;
		vtt: TimeHelperFormat;
	};

	export interface ParseOptions {
		format?: SupportedFormats;
		verbose?: boolean;
		eol?: string;
		fps?: number;
		preserveSpaces?: boolean;
	}

	type CaptionLine = {
		type: string;
		index: number;
		start: number;
		end: number;
		duration: number;
		content: string;
		text: string;
	};

	type Captions = CaptionLine[];

	export interface ResyncOptions {
		offset?: number;
		ratio?: number;
		frame?: boolean;
	}

	export interface BuildOptions {
		format: SupportedFormats;
		verbose?: boolean;
		fps?: number;
		closeTags?: boolean;
	}

	export interface ConvertOptions {
		format: SupportedFormats;
		verbose?: boolean;
		eol?: string;
		fps?: number;
		resync?: ResyncOptions;
	}

	export function build(captions: Captions, options: BuildOptions): string;

	export function convert(content: string, options: ConvertOptions): string;

	export function detect(content: string): SupportedFormats;

	export function list(): SupportedFormats[];

	export function parse(content: string, options: ParseOptions): Captions;

	export function resync(captions: Captions, options: ResyncOptions): Captions;
}
