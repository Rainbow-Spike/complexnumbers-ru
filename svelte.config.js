import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [
		preprocess({
			postcss: true,
			preserve: ['ld+json'],
		}),
	],
	kit: {
		adapter: adapter({
			pages: 'public',
			assets: 'public',
		}),

		prerender: {
			default: true,
		},

		trailingSlash: 'always',
	},
};

export default config;
